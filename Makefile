PY = python3
NASM = nasm
LD = ld
RM = rm
N_FLAGS = -felf64 -o
EXECUTABLE = lab2

%.o: %.asm	
	$(NASM) $(N_FLAGS) $@ $<

$(EXECUTABLE): main.o lib.o dict.o
	$(LD) -o $(EXECUTABLE) $^

clean: 
	$(RM) -f dict.o lib.o main.o main

test:
	$(PY) ./test.py

.PHONY: clean test
