global _start

%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUF_SIZE 256

section .bss
buf: resb BUF_SIZE

section .rodata
invite: db "Enter dictionary key:", 0xA, 0
readingError: db "Error while reading a word", 0
findingError: db "Error during dictionary search. Check the entered word", 0

section .text
_start:
    mov rdi, invite
    call print_string

    mov rdi, buf
    mov rsi, BUF_SIZE

    call read_word
    test rax, rax
    je .reading_error

    mov rdi, rax
    mov rsi, firstKey

    call find_word
    test rax, rax
    je .finding_error

    mov rdi, rax
    add rdi, 8

    call string_length
    
    inc rdi
    add rdi, rax
    call print_string

    xor rdi, rdi
    jmp .exit

.reading_error:
    mov rdi, readingError
    call print_error
    mov rdi, 1
    jmp .exit

.finding_error:
    mov rdi, findingError
    call print_error
    mov rdi, 1

.exit:
    jmp exit
