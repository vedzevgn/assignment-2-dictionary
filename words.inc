%include "colon.inc"


section .data

colon "Test", testKey
db "This is the test string for the test key :)", 0

colon "Cat", catKey
db "/ᐠ. ｡.ᐟ\ᵐᵉᵒʷˎˊ˗", 0

colon "anotherCat", anotherCatKey
db "(=🝦 ﻌ 🝦=)", 0

colon "First", firstKey
db "This is the first string!", 0

