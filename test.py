# -*- coding: utf-8 -*-

import subprocess
import re
import os
import unittest
from subprocess import CalledProcessError

class MyTest(unittest.TestCase):
    keys = ["First", "Cat", "anotherCat", "Test", "WrongKey"]
    expected_results = [
        "This is the first string!",
        "/ᐠ. ｡.ᐟ\ᵐᵉᵒʷˎˊ˗",
        "(=🝦 ﻌ 🝦=)",
        "This is the test string for the test key :)",
        ""
    ]

    def test_strings(self):
        for key, expected_result in zip(self.keys, self.expected_results):
            popen = subprocess.Popen(["./lab2"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, error = popen.communicate(input=key.encode("utf-8"))
            out = out.strip()
            self.assertTrue(out.strip().decode("utf-8").lstrip("Enter dictionary key:\n") == expected_result.strip(), "Test failed with key: '" + key + "'. Expected: '" + expected_result.strip() + "', returned: '" + out.strip().decode("utf-8").lstrip("Enter dictionary key:\n") + "'")

if __name__ == "__main__":
    if os.path.exists("./lab2"):
        unittest.main()

