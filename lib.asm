global exit
global string_length
global print_error
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy 


%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0
%define STDERR 2
%define STDOUT 1
%define STDIN 0

%define SPACE 0x20
%define TAB 0x9
%define NEW_LINE 0xA
%define ZERO '0'
%define NINE '9'
%define MINUS '-'


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .counter:
        cmp byte [rax + rdi], 0
        je .end
        inc rax
        jmp .counter

    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, STDOUT


print_std:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    syscall
    ret


print_error:
    mov rsi, STDERR
    call print_std
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, NEW_LINE
    call print_char
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp 
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 0
    sub rsp, 8
    mov byte [rsp], 0
    mov rax, rdi
    mov r10, NEW_LINE

    .loop:
        mov rdx, 0
        div r10             ; convert number
        add dl, ZERO        ; to ASCII
        inc rcx
        dec rsp
        mov [rsp], dl
        test rax, rax
        jne .loop

    .print:
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        add rsp, 8
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, MINUS        ; printing a minus sign if the number is negative
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx

    .check:
        mov dh, byte[rdi + rcx]
        cmp dh, byte[rsi + rcx]
        jne .done
        test dh, dh
        je .true
        inc rcx
        jmp .check

    .true:
        mov rax, 1

    .done:
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

read_char:
    mov rax, SYS_READ
    mov rdi, STDIN
    mov rdx, 1

    push 0
    mov rsi, rsp
    syscall
    test rax, rax
    jle .stop
    mov rax, qword[rsp]

.stop:
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx
    push r12
    push r13
    mov rbx, 0          ; counter for symbols
    dec rsi
    mov r13, rsi
    mov r12, rdi

    .loop:
        call read_char
        test rax, rax       ; checking for the end
        jle .break
        cmp rax, SPACE      ; checking for a space (0x20)
        je .reset
        cmp rax, TAB        ; checking for tabs (0x9)
        je .reset
        cmp rax, NEW_LINE   ; checking for line feed (0xA)
        je .reset
        cmp rbx, r13
        jge .error

        mov [r12 + rbx], al
        inc rbx
        jmp .loop

    .error:
        xor rax, rax
        jmp .exit 

    .reset:
        test rbx, rbx
        je .loop  

    .break:
        mov byte[r12 + rbx], 0
        mov rax, r12
        mov rdx, rbx

    .exit:
        pop r13
        pop r12
        pop rbx
        ret




read_word:               
    push r12
    push r13
    test rsi, rsi
    je   .error

    xor  rbx, rbx
    mov  r12, rdi
    mov  r13, rsi
    xor  r9, r9

    .read_space:
        call read_char
        test rax, rax
        jz   .error
        call .check_space
        cmp  rdx, 1         ; checking the next character
        jb   .read_space
        ja   .error

    .start_read:
        inc  r9
        cmp  r9, r13
        jnb  .error
        mov  [r12], rax
        inc  r12
        call read_char
        call .check_space
        cmp  rdx, 1
        je   .start_read

    .success:
        mov  byte [r12], 0
        mov  rax, r12
        sub  rax, r9
        
        push rdi
        push rax
        mov  rdi, rax
        call string_length
        mov  rdx, rax
        pop  rax
        pop  rdi

        jmp  .exit

    .check_space:       ; checking spaces for skip
        xor  rdx, rdx
        cmp  rax, ` `
        je   .skip
        cmp  rax, `\n`
        je   .skip
        cmp  rax, `\t`
        je   .skip
        inc  rdx
        test rax, rax
        jnz  .skip
        inc  rdx
        
    .error:
        xor rax, rax
        xor rdx, rdx

    .exit:
        pop  r13
        pop  r12

    .skip:
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    cmp byte [rdi], 0
    je .end

    .loop:
        cmp byte [rdi], ZERO
        jl .end                 ; if the character is less than '0' then exit the function
        cmp byte [rdi], NINE
        jg .end                 ; if the character is greater than '9' then exit the function

        sub byte [rdi], ZERO    ; ASCII to number
        imul rax, 10
        add al, byte [rdi]
        inc rdi                 ; go to the next digit
        inc rdx
        jmp .loop

    .end:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], MINUS
    jne parse_uint
	inc rdi
	call parse_uint
	test rdx, rdx      ; checking the correctness of the number
	je .error
	inc rdx            ; adding a minus sign to the total number of characters
	neg rax
	ret
    
    .error:
        xor rax, rax
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    
    cmp rax, rdx                   ; checking whether the length of the string exceeds the size of the array
    jg .error
    xor rcx, rcx

    .loop:
        mov r10b, byte[rdi + rcx]
        mov byte[rsi + rcx], r10b
        cmp rcx, rax
        jz .end
        inc rcx
        jmp .loop

    .error:
        xor rax, rax

    .end:
        ret
