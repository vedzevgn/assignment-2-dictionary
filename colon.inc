%define address 0

%macro colon 2
	%2: dq address
	db %1, 0
	%define address %2
%endmacro
