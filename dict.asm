global find_word

%include "lib.inc"

section .text

; Ищет вхождения указанного ключа в словаре
; Если подходящее вхождение найдено, возвращает адрес начала вхождения в словарь, иначе возвращает 0
find_word:
    test rdi, rdi
    je .error

.loop:
    test rsi, rsi
    je .error

    add rsi, 8
    call string_equals

    cmp rax, 1
    je .success
    
    sub rsi, 8
    mov rsi, [rsi]
    jmp .loop

.success:
    mov rax, rsi
    ret

.error:
    xor rax, rax
    ret
